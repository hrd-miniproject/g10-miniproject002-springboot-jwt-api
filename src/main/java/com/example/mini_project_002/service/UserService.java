package com.example.mini_project_002.service;

import com.example.mini_project_002.model.User;
import com.example.mini_project_002.model.request.UpdateProfileRequest;
import com.example.mini_project_002.model.response.UserResponse;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    //getAllUser
    List<User> getAllUser(int limit, int offset, String filter);
    UserResponse findUserByUsername(String username);

    int register(User user);

    User finUserById(int id);

    int disableAccount(int id);

    int updateProfile(int id, UpdateProfileRequest request);
}
