package com.example.mini_project_002.service.serviceImp;

import com.example.mini_project_002.model.FileStorageProperties;
import com.example.mini_project_002.service.FileStorageService;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileStorageServiceImp extends FileStorageService {


    public FileStorageServiceImp(FileStorageProperties fileStorageProperties) {
        super(fileStorageProperties);
    }

    @Override
    public String storeFile(MultipartFile file) {
        return null;
    }

    @Override
    public Resource loadFileAsResource(String fileName) {
        return null;
    }
}
