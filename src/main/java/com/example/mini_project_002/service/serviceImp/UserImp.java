package com.example.mini_project_002.service.serviceImp;

import com.example.mini_project_002.model.User;
import com.example.mini_project_002.model.request.UpdateProfileRequest;
import com.example.mini_project_002.model.response.UserResponse;
import com.example.mini_project_002.repository.RoleRepository;
import com.example.mini_project_002.repository.UserRepository;
import com.example.mini_project_002.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserImp implements UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    private RoleRepository roleRepository;

    @Autowired
    public UserImp(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<User> getAllUser(int limit, int offset, String filter) {
        return userRepository.getAllUser(limit,offset, filter);
    }

    @Override
    public UserResponse findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public int register(User user) {
        String encryptedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptedPassword);
        return userRepository.register(user);
    }

    @Override
    public User finUserById(int id) {
        return userRepository.finUserById(id);
    }

    @Override
    public int disableAccount(int id) {
        return userRepository.disableAccount(id);
    }

    @Override
    public int updateProfile(int id, UpdateProfileRequest request) {
        return userRepository.updateProfile(id, request);
    }

}
