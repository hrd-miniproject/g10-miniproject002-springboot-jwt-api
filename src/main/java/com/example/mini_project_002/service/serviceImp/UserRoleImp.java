package com.example.mini_project_002.service.serviceImp;

import com.example.mini_project_002.repository.RoleRepository;
import com.example.mini_project_002.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleImp implements UserRoleService {
    RoleRepository roleRepository;

    @Autowired
    public UserRoleImp(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public int insertUserRole(int userId, int roleId) {
        return roleRepository.insertUserRole(userId, roleId);
    }
}
