package com.example.mini_project_002.service.serviceImp;

import com.example.mini_project_002.model.Comment;
import com.example.mini_project_002.model.Post;
import com.example.mini_project_002.repository.CommentRepository;
import com.example.mini_project_002.repository.PostRepository;
import com.example.mini_project_002.service.PostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostImp implements PostService {
    private PostRepository postRepository;
    private CommentRepository commentRepository;
    @Autowired
    public PostImp(PostRepository postRepository, CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public int insertPost(Post post, int id) {
        return postRepository.insertPost(post, id);
    }

    @Override
    public int updatePost(Post post, int id) {
        return postRepository.updatePost(post, id);
    }

    @Override
    public Post findPostById(int id) {
        return postRepository.findPostById(id);
    }

    @Override
    public List<Post> getAllPost(int limit, int offset, String filter) {
        return postRepository.getAllPost(limit, offset, filter);
    }

    @Override
    public int insertComment(String content, int post_id, int user_id) {
        return commentRepository.insertComment(content, post_id, user_id);
    }

    @Override
    public int insertReply(String content, int comment_id,int post_id, int user_id) {
        return commentRepository.insertReply(content,comment_id,post_id,user_id);
    }

    @Override
    public Comment findCommentById(int id) {
        return commentRepository.findCommentById(id);
    }
}
