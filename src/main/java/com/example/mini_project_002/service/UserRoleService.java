package com.example.mini_project_002.service;

import org.springframework.stereotype.Service;

@Service
public interface UserRoleService {
    int insertUserRole(int userId, int roleId);
}
