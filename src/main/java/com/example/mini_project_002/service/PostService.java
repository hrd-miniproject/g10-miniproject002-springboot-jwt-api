package com.example.mini_project_002.service;

import com.example.mini_project_002.model.Comment;
import com.example.mini_project_002.model.Post;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {
    public int insertPost(Post post, int id);
    int updatePost(Post post, int id);
    public Post findPostById(int id);

    public List<Post> getAllPost(int limit, int offset, String filter);

//    comment
int insertComment(String content, int post_id, int user_id);

    int insertReply(String content, int comment_id,int post_id, int user_id);

    Comment findCommentById(int id);


}
