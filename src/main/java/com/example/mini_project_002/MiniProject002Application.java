package com.example.mini_project_002;

import com.example.mini_project_002.model.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication

@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class MiniProject002Application {

    public static void main(String[] args) {
        SpringApplication.run(MiniProject002Application.class, args);
    }

}
