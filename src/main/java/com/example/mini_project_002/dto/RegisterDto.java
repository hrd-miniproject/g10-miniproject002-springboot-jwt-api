package com.example.mini_project_002.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegisterDto {
    private String fullName;
    private List<String> roles;
    private String profile;
    private String username;
}
