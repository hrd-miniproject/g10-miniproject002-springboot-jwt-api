package com.example.mini_project_002.dto.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserLoginResponse {
    private String token;
    private int id;
    private String username;
    private String fullName;
    private List<String> roles;
}
