package com.example.mini_project_002.dto;


import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiError {
    private String message ;
    private HttpStatus status;
}
