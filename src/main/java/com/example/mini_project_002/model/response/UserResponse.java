package com.example.mini_project_002.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserResponse {
    private int id;
    private String username;
    private String profile;
    private String fullName;
    private List<String> roles;
}
