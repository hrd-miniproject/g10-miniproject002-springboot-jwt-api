package com.example.mini_project_002.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UploadFileResponse {
    private String fileName;
    private String fileUrl;
    private String fileType;
    private long size;
}
