package com.example.mini_project_002.model.response;

import com.example.mini_project_002.model.Comment;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PostResponse {
    private int id;
    private String caption;
    private String image;
    private int userId;
    private int numOfPostLikes;
    private List<Comment> comments;
    private boolean owner;
    private String username;
    @JsonFormat (pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createdAt;
    @JsonFormat (pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updatedAt;

}
