package com.example.mini_project_002.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Comment {
    private int id;
    private String text;
    private int postId;
    private int parentId;
    private int numOfLike;
    private int userId;
    private List<Comment> replies;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createdAt;
    @JsonFormat (pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updatedAt;
}
