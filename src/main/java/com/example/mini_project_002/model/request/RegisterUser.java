package com.example.mini_project_002.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegisterUser {
    private String fullName ;
    private String password;
    private String profile;
    private List<String> roles;
    private String username ;
}
