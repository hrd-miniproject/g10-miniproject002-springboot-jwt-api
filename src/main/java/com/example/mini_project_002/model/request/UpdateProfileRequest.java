package com.example.mini_project_002.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateProfileRequest {
    private String fullName ;
    private String profile;
    private String username ;
}
