package com.example.mini_project_002.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
    private int id;
    private String fullName;
    private String username;
    private List<String> roles;
    private String password;
    private String profile;
    private boolean status;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createdAt;
    @JsonFormat (pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updatedAt;

}
