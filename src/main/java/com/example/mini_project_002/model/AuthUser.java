package com.example.mini_project_002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthUser {
    private int id ;
    private String fullName;
    private String username ;
    private String password;
    private String profile;
    private List<String> roles;
}
