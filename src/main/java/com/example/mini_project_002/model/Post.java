package com.example.mini_project_002.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Post {
    private int id;
    private String caption;
    private String image;
    private int userId;
    private int numOfLike;
    private List<Comment> comments;
    private boolean owner;
    private String username;
    @JsonFormat (pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createdAt;
    @JsonFormat (pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updatedAt;

}
