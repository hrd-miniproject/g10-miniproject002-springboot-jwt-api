package com.example.mini_project_002.repository;

import com.example.mini_project_002.model.Post;
import com.example.mini_project_002.repository.provider.PostProvider;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PostRepository {


    @SelectProvider(type = PostProvider.class, method = "getAllPost")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "caption", column = "caption"),
            @Result(property = "image", column = "image"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "numOfLike", column = "id", many = @Many(select = "com.example.mini_project_002.repository.NumOfPostLikeRepository.findNumOfPostLikeByPostId")),
            @Result(property = "comments", column = "id", many = @Many(select = "com.example.mini_project_002.repository.CommentRepository.findAllCommentByUserId")),
            @Result(property = "username", column = "user_id", many = @Many(select = "com.example.mini_project_002.repository.repository.UserRepository.findUserNameById")),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at")
    })
    public List<Post> getAllPost(int limit, int offset, String filter);
    @Select("insert into post(caption, image, user_id) values(#{post.caption},#{post.image},#{id}) returning id")
    public int insertPost(@Param("post") Post post, int id);

    @Update("update post set caption = #{post.caption}, image = #{post.image} where id = #{id}")
    public int updatePost(@Param("post") Post post, int id);

    @Select("select * from post where id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "caption", column = "caption"),
            @Result(property = "image", column = "image"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "numOfLike", column = "id", many = @Many(select = "com.example.mini_project_002.repository.NumOfPostLikeRepository.findNumOfPostLikeByPostId")),
            @Result(property = "comments", column = "id", many = @Many(select = "com.example.mini_project_002.repository.CommentRepository.findAllCommentByUserId")),
            @Result(property = "username", column = "user_id", many = @Many(select = "com.example.mini_project_002.repository.UserRepository.findUserNameById")),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at")
    })
    public Post findPostById(int id);


}
