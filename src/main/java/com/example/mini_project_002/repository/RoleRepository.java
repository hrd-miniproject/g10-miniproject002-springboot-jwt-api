package com.example.mini_project_002.repository;

import com.example.mini_project_002.model.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RoleRepository {
    @Select("select r.id, r.role_name, r.created_at, r.updated_at from role r inner join user_role ur on r.id = ur.role_id where ur.user_id = #{id}")
    @Results(
            {
                @Result(property = "id", column = "id"),
                @Result(property = "roleName", column = "role_name"),
                @Result(property = "createdAt", column = "created_at"),
                @Result(property = "updatedAt", column = "updated_at")
            }
    )
     List<Role> findRoleById(int id);

    @Select("select r.role_name from role r inner join user_role ur on r.id = ur.role_id where ur.user_id = #{id}")
    List<String> findRoleNameById(int id);

    @Select("insert into user_role(role_id, user_id) values(#{roleId}, #{userId}) returning id")
    int insertUserRole(int userId, int roleId);
}
