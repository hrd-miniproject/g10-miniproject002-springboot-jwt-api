package com.example.mini_project_002.repository;

import com.example.mini_project_002.model.Comment;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CommentRepository {

    @Select("select * from comment where post_id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "text", column = "text"),
            @Result(property = "numOfLike", column = "post_id", many = @Many(select = "com.example.mini_project_002.repository.NumOfCommentLikeRepository.findNumOfCommentLikeByPostId")),
            @Result(property = "replies", column = "parent_id", many = @Many(select = "findAllReplyByUserId")),
            @Result(property = "postId", column = "post_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at")
    })
    public List<Comment> findAllCommentByUserId(int id);

//    findAllReply
@Select("select * from comment where parent_id=#{id}")
@Results({
        @Result(property = "id", column = "id"),
        @Result(property = "text", column = "text"),
        @Result(property = "numOfLike", column = "post_id", many = @Many(select = "com.example.mini_project_002.repository.NumOfCommentLikeRepository.findNumOfCommentLikeByPostId")),
        @Result(property = "postId", column = "post_id"),
        @Result(property = "userId", column = "user_id"),
        @Result(property = "parentId", column = "parent_id"),
        @Result(property = "createdAt", column = "created_at"),
        @Result(property = "updatedAt", column = "updated_at")
})
public List<Comment> findAllReplyByUserId(int id);

//    insertComment
    @Select("insert into comment(text, post_id, user_id) values(#{content}, #{post_id}, #{user_id}) returning id")
    int insertComment(String content, int post_id, int user_id);

    @Select("insert into comment(text, post_id, parent_id, user_id) values(#{content}, #{post_id}, #{comment_id}, #{user_id}) returning post_id")
    int insertReply(String content, int comment_id,int post_id, int user_id);

    @Select("select * from comment where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "text", column = "text"),
            @Result(property = "numOfLike", column = "post_id", many = @Many(select = "com.example.mini_project_002.repository.NumOfCommentLikeRepository.findNumOfCommentLikeByPostId")),
            @Result(property = "replies", column = "parent_id", many = @Many(select = "findAllReplyByUserId")),
            @Result(property = "postId", column = "post_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at")
    })
    Comment findCommentById(int id);
}
