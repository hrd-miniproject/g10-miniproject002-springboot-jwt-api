package com.example.mini_project_002.repository;

import com.example.mini_project_002.model.AuthUser;
import com.example.mini_project_002.model.User;
import com.example.mini_project_002.model.request.UpdateProfileRequest;
import com.example.mini_project_002.model.response.UserResponse;
import com.example.mini_project_002.repository.provider.UserProvider;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserRepository {
//getAllUser
    @SelectProvider(type = UserProvider.class, method = "getAllUser")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "fullName", column = "full_name"),
            @Result(property = "password", column = "password"),
            @Result(property = "profile", column = "profile"),
            @Result(property = "username", column = "username"),
            @Result(property = "status", column = "status"),
            @Result(property = "roles", column = "id", many = @Many(select = "com.example.mini_project_002.repository.RoleRepository.findRoleNameById")),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at")
    })
    public List<User> getAllUser(int limit, int offset,String filter);

    @Select("select full_name from users where id = #{id} ")
    public String findUserNameById(int id);

    @Select("select * from users where  username = #{username}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "username", column = "username"),
            @Result(property = "fullName", column = "full_name"),
            @Result(property = "profile", column = "profile"),
            @Result(property = "roles", column = "id", many = @Many(select = "com.example.mini_project_002.repository.RoleRepository.findRoleNameById")),
    })
    AuthUser findUserByUsername(String username);

    @Select("select * from users where  username = #{username}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "username", column = "username"),
            @Result(property = "fullName", column = "full_name"),
            @Result(property = "profile", column = "profile"),
            @Result(property = "roles", column = "id", many = @Many(select = "com.example.mini_project_002.repository.RoleRepository.findRoleNameById")),
    })
    UserResponse findByUsername(String username);

    @Select("insert into users(full_name, username, password, profile, status) values(#{user.fullName}, #{user.username}, #{user.password}, #{user.profile}, #{user.status}) returning id")
    int register(@Param("user") User user);

//    findUserById
    @Select("select * from users where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "fullName", column = "full_name"),
            @Result(property = "password", column = "password"),
            @Result(property = "profile", column = "profile"),
            @Result(property = "username", column = "username"),
            @Result(property = "status", column = "status"),
            @Result(property = "roles", column = "id", many = @Many(select = "com.example.mini_project_002.repository.RoleRepository.findRoleNameById")),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at")
    })
    User finUserById(int id);

    @Update("update users set status = false where id = #{id}")
    int disableAccount(int id);

    @Update("update users set full_name = #{request.fullName}, profile = #{request.profile}, username = #{request.username} where id = #{id}")
    int updateProfile(int id, @Param("request") UpdateProfileRequest request);

}
