package com.example.mini_project_002.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {
    public String getAllUser(int limit, int offset, String filter) {
        return new SQL() {{
            System.out.println("Filter value : " + filter);
            if (!filter.isEmpty()) {
                System.out.println(" Filter is not empty...");
                SELECT("*");
                FROM("users");
                LIMIT("#{limit}");
                OFFSET("#{offset}");
                WHERE("UPPER (full_name) like upper('%'||#{filter}||'%')");
            } else {
                SELECT("*");
                FROM("users");
                LIMIT("#{limit}");
                OFFSET("#{offset}");

            }
        }}.toString();
    }
}
