package com.example.mini_project_002.controller;

import com.example.mini_project_002.model.response.UserResponse;
import com.example.mini_project_002.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    @Autowired
    UserRepository userRepository;

    @GetMapping("index")
    public UserResponse test(){
        return userRepository.findByUsername("admin");
    }
}
