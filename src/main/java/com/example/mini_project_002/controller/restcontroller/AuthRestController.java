package com.example.mini_project_002.controller.restcontroller;

import com.example.mini_project_002.dto.RegisterDto;
import com.example.mini_project_002.dto.request.UserLoginRequest;
import com.example.mini_project_002.dto.response.UserLoginResponse;
import com.example.mini_project_002.model.User;
import com.example.mini_project_002.model.request.RegisterUser;
import com.example.mini_project_002.model.response.UserResponse;
import com.example.mini_project_002.security.UserDetailImp;
import com.example.mini_project_002.security.jwt.JwtUtils;
import com.example.mini_project_002.service.UserRoleService;
import com.example.mini_project_002.service.UserService;
import com.example.mini_project_002.utils.response.Response;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthRestController {

private UserService userService;
    private AuthenticationManager authenticationManager;
    private UserRoleService userRoleService;

    @Autowired
    public AuthRestController(UserService userService, AuthenticationManager authenticationManager, UserRoleService userRoleService) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.userRoleService = userRoleService;
    }

    ModelMapper modelMapper = new ModelMapper();
    @PostMapping("/login")
    Response<UserLoginResponse> login (@RequestBody UserLoginRequest request){

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(), request.getPassword()
                );
        try{
            Authentication authentication = authenticationManager.authenticate(authenticationToken);

            JwtUtils jwtUtils  = new JwtUtils();
            // jwtToken
            String token = jwtUtils.generateJwtToken(authentication);

            UserLoginResponse response = new UserLoginResponse();

            try{

                UserResponse findUserByUsername = userService.findUserByUsername(request.getUsername());
                response =modelMapper.map(findUserByUsername,UserLoginResponse.class);
               //response.setUsername(findUserByUsername.getUsername());


            }catch (Exception exception){
                System.out.println("Error when finding the user by username");
                return Response.<UserLoginResponse>exception().setError("Failed to find the user by the provided username !");

            }

          response.setToken(token);

            return Response.<UserLoginResponse>ok().setPayload(response).setError("Login successfullly");
        }catch (Exception exception){
            System.out.println("Exception occur when trying to login for this credential : "+ exception.getMessage());

       return Response.<UserLoginResponse>exception().setError("Failed to perform the authentication due the the exception issues !");

        }


    }

    @PostMapping("/signup")
    public Response<RegisterDto> signup(@RequestBody RegisterUser user, @AuthenticationPrincipal UserDetailImp login){
        try{
            User user1 = new User();
            user1.setFullName(user.getFullName());
            user1.setPassword(user.getPassword());
            user1.setStatus(true);
            user1.setRoles(user.getRoles());
            user1.setProfile(user.getProfile());
            user1.setUsername(user.getUsername());
            // insert
            int newInsertedId = userService.register(user1);
            System.out.println("here is insert id: " + newInsertedId);
            user.getRoles().stream().forEach(item->{
                if(item.contains("admin"))
                    userRoleService.insertUserRole(newInsertedId, 1);
                else  {
                    userRoleService.insertUserRole(newInsertedId, 2);
                }
            });

            User newUser = userService.finUserById(newInsertedId);
            System.out.println("here is newPost: "+ newUser);
            // map
            RegisterDto response = modelMapper.map(newUser,RegisterDto.class);

            return Response.<RegisterDto>successCreate().setPayload(response).setError("Created Successfully!").setSuccess(true);

        }catch (Exception ex){
            System.out.println("Insert user Exception ; "+ex.getMessage());
            String message = "";
                if(ex.getMessage().contains("duplicate")){
                    message = "username is already exist!";
                }
            return Response.<RegisterDto>exception().setError("Failed to register user due to the sql exception " + message);

        }

    }


}
