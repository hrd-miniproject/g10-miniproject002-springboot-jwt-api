package com.example.mini_project_002.controller.restcontroller;

import com.example.mini_project_002.model.Comment;
import com.example.mini_project_002.model.Post;
import com.example.mini_project_002.model.request.PostRequest;
import com.example.mini_project_002.security.UserDetailImp;
import com.example.mini_project_002.service.PostService;
import com.example.mini_project_002.utils.paging.Paging;
import com.example.mini_project_002.utils.response.Response;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/posts")
public class PostController {
    private PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

//    create post
    @PostMapping("/create-post")
    public Response<Post> createPost(@RequestBody PostRequest postRequest, @AuthenticationPrincipal UserDetailImp login){

        return null;
    }

    //   update post
    @PatchMapping("/{id}/update-post")
    public Response<Post> updateResponse(@RequestBody PostRequest postRequest, @PathVariable int id){
    return  null;
    }

//    getAllPost
@GetMapping("/findAllPosts")
public Response<List<Post>> findAllPosts(@RequestParam(defaultValue ="10") int limit, @RequestParam(defaultValue = "1")  int page , @RequestParam(defaultValue = "") String filter){
    List<Post> findAllPosts = new ArrayList<>();
    try {
        Paging paging =  new Paging();
        paging.setPage(page);
        int offset = (page -1)* limit;
        paging.setLimit(limit);

        // totalCount
//            findAllUsers = UserService.getAllUser();
        findAllPosts = postService.getAllPost(limit, offset,filter);
        return Response.<List<Post>>ok().setPayload(findAllPosts).setError("Posts fetched successfully!").setMetadata(paging);

    }catch (Exception ex){
        System.out.println(" Fetching data exception: "+ex.getMessage());
        return Response.<List<Post>>exception().setError("Failed to retrieve the Posts due the sql exception...");
    }
}

enum  ReactionType {
        LIKE,UNLIKE
}
@PatchMapping("/{id}/react")
public Response<Post> reactPost(@PathVariable int id, @RequestParam ReactionType type){

return null;
}

//    insert comment
    @PostMapping("/{post_id}/comment")
    public Response<Post> Comment(@RequestParam String content, @PathVariable int post_id, @AuthenticationPrincipal UserDetailImp login){

        return  null;
    }

//    insert reply
@PostMapping("/{comment_id}/reply")
public Response<Post> Reply(@RequestParam String content, @PathVariable int comment_id, @AuthenticationPrincipal UserDetailImp login){

    return  null;
}

@GetMapping("/filterPost")
    public Response<Post> filterPost(){

        return null;
}





}
