package com.example.mini_project_002.controller.restcontroller;

import com.example.mini_project_002.model.User;
import com.example.mini_project_002.model.request.UpdateProfileRequest;
import com.example.mini_project_002.model.response.UserResponse;
import com.example.mini_project_002.service.UserService;
import com.example.mini_project_002.utils.response.Response;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/account")
public class AccountRestController {
    private UserService userService;
    @Autowired
    public AccountRestController(UserService userService) {
        this.userService = userService;
    }
    ModelMapper modelMapper = new ModelMapper();
    @PatchMapping("/{id}/close")
    Response<UserResponse> disableAccount(@PathVariable int id){
        try {
            int newId = 0;
            try {
                userService.disableAccount(id);
            }catch (Exception e){
                e.printStackTrace();
            }
            User newUser = userService.finUserById(id);
            UserResponse userResponse = modelMapper.map(newUser, UserResponse.class);
            System.out.println("here is userResponse " + userResponse);
            return Response.<UserResponse>successCreate().setPayload(userResponse).setError("Account disable successfully").setSuccess(true);
        }catch (Exception e){
            System.out.println("Disable Account error " + e.getMessage());
            return Response.<UserResponse>exception().setError("Error to disable account");
        }
    }

    @PatchMapping("/{id}/update-profile")
    Response<UserResponse> updateProfile(@PathVariable int id, @RequestBody UpdateProfileRequest request){
        try {
            userService.updateProfile(id, request);
            User newUser = userService.finUserById(id);
            UserResponse userResponse = modelMapper.map(newUser, UserResponse.class);
            System.out.println("here is userResponse " + userResponse);
            return Response.<UserResponse>successCreate().setPayload(userResponse).setError("Account update successfully").setSuccess(true);
        }catch (Exception e){
            System.out.println("Update Account error " + e.getMessage());
            return Response.<UserResponse>exception().setError("Error to update account");
        }
    }
}
